class SiteController < ApplicationController

	add_breadcrumb "ГЛАВНАЯ", :root_path

	def index
		@collections = Complect.order('id DESC')
	end

	def complects
		add_breadcrumb 'Комплекты мебели', '/complects'
		@collections = Complect.order('id DESC')
		render :index
	end

	def item_details
		@item = Item.find(params[:item_id]) rescue nil
		if @item.nil?
			redirect_to '/404'
			return
		end
		add_breadcrumb 'Продукция', '/catalog'
		add_breadcrumb @item.name, '/catalog/'+@item.id.to_s
		@similar_items = Item.includes(:category).where(category_id: @item.category_id)
	end

	def catalog
		add_breadcrumb 'Продукция', '/catalog'
		if params[:category_id].blank?
			if params[:complect_id].blank?
				@items = Item.order('price asc')
			else
				@items = Item.where(complect_id: params[:complect_id]).order('price asc')
				add_breadcrumb Complect.find(params[:complect_id]).name
			end 
		else
			@items = Item.where(category_id: params[:category_id]).order('price asc')
			add_breadcrumb  Category.find(params[:category_id]).name
		end
	end

	def cart
	end

	def order
		add_breadcrumb 'Корзина', '/cart'
		add_breadcrumb 'Оформление заказа', '/order'
		if params[:order]
			params.permit!
			@order = Order.new(params[:order])
			if @order.valid?
				@order.content = ''
				session[:cart].each do |i|
					item = Item.find(i[0])
					@order.content = @order.content + "ID:"+i[0].to_s+" Название: "+item.name+" "+item.width.to_s+" см - "+i[1].to_s+" шт;" 
				end
				@order.save
				SiteMailer.send_order(@order).deliver
			end
		else	
			@order = Order.new
		end
	end

	def vacancy
		add_breadcrumb 'Вакансии', '/vacancy'
		@items = Vacancy.order('id DESC')
		@collections = Complect.order('id DESC')
	end

	def textpage
		page = Textpage.where(slug: params[:url])
		@header = page[0].name
		@content = page[0].content
		add_breadcrumb page[0].name, '/'+page[0].slug
	end


	def shops
		add_breadcrumb 'Наши магазины', '/shops'
		@shops = Shop.order('id DESC')
		@collections = Complect.order('id DESC')
	end

	def search
		if params[:q]
			add_breadcrumb 'Результаты поиска'
			@items = Item.search params[:q], :per_page => 1000
			render :search
			return
		end
		redirect_to '/'
		return
	end

	def surprice
		add_breadcrumb 'Хиты продаж и скидки'
		@items = Item.where('hit =1 OR old_price <> 0').order('price DESC')
	end

	def news 
		add_breadcrumb 'Новости', '/news'
		if !params[:slug]
			@news = News.order('id DESC')
			render :news
		else
			@item = News.where('alias = ?', params[:slug]) rescue nil
			if @item == nil
				redirect_to '/404'
				return
			else
				@header = @item[0].title
				@content = @item[0].content
				add_breadcrumb @header
				render :textpage
			end
		end
	end

	def contacts
		add_breadcrumb 'Контакты', '/contacts'
	end
end
