class AjaxController < ApplicationController
	def get_items
		items = Item.select('items.*, categories.name as cat_name').
			joins("INNER JOIN categories ON items.category_id = categories.id").
			where("1 "+params[:query_str])
		if !params[:category_id].blank?
			items = items.where(category_id: params[:category_id])
		end
		if !params[:complect_id].blank?
			items = items.where(complect_id: params[:complect_id])
		end
		items = items.order(params[:order])
		render :json => {count: items.size, items: items}
	end

	def add_to_cart
		if session[:cart].blank?
			session[:cart] = Hash.new
		end

		if session[:cart][params[:item_id]].blank?
			start_count = 0
		else
			start_count = session[:cart][params[:item_id]]
		end

		session[:cart][params[:item_id]] = start_count + 1

		render :json => params[:item_id]
	end

	def del_from_cart
		if params[:item_id] == 'all'
			reset_session
			render :json => params[:items_id]
			return
		else
			session[:cart][params[:item_id]] = nil
			cart_empty = true
			session[:cart].each do |i|
				if !i[1].nil?
					cart_empty = false
				end
			end
			reset_session if cart_empty
			render :json => session[:cart]
		end
	end

	def ch_count
		if params[:count].to_i < 0
			new_count = 0
		else
			new_count = params[:count]
		end
		session[:cart][params[:item_id]] = new_count.to_i
		render :json => {item_id: params[:item_id], new_count: new_count, sess: session[:cart]}
		return
	end
end