class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :construct

  private
		def construct
			@news_widget = News.order('id DESC').limit(3)
			@menu_items = Menu.where(:hide => 0, :parent => nil).order('ordernum ASC')
			@hits_and_discounts = Item.where("hit = true OR old_price").order('RAND()').limit(6)
			
			if params[:getcall_footer]
				@getcall_footer = Getcall_footer.new(params[:getcall_footer])
				if @getcall_footer.valid?
					SiteMailer.getcall_email(@getcall_footer.email, @getcall_footer.message).deliver
				end
			else
				@getcall_footer = Getcall_footer.new
			end

			if params[:getcall]
				@getcall = Getcall.new(params[:getcall])
				if @getcall.valid?
					SiteMailer.getcall_email_popup(@getcall).deliver
				end
			else
				@getcall = Getcall.new
			end
		end
end
