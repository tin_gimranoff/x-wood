var query = '';
var checkbox_arr = new Object();
checkbox_arr['width'] = [];
checkbox_arr['collection'] = [];
checkbox_arr['construction'] = [];
checkbox_arr['mounting'] = [];
var order = 'price asc';

$(document).ready(function(){

	$(".download-catalog *").bind('mouseover', function(){
		$(".download-catalog-img-link img").hide();
	});
	$(".download-catalog *").bind('mouseout', function(){
		$(".download-catalog-img-link img").show();
	});

	$('#slider').bxSlider({
	  minSlides: 1,
	  maxSlides: 2,
	  slideWidth: 1025,
	  slideMargin: 0,
	  pager: false,
	});

	var left_slider_position = 1025-((($(window).outerWidth()-1025)/2));
	$(".bx-viewport").css('left', '-'+left_slider_position+'px').css('width', $(".bx-viewport").width()+left_slider_position);
	$(".left-slider-mask").css('width',($(window).outerWidth()-1025)/2);
	$(".right-slider-mask").css('width',($(window).outerWidth()-1025)/2);
	$(".bx-prev").css('margin-left', ($(window).outerWidth()-1025)/2);
	$(".bx-next").css('margin-right', ($(window).outerWidth()-1025)/2);


    $(".checkbox-item").bind('click', function(){
        var field = $(this).parent().attr('data-field');
        if(field == 'collection')
            var value = $(this).attr('data-value');
        else
            var value = $(this).find(".checkbox-label").text();
        if($(this).find(".checkbox-checked").css('display') == 'none')
        {
            $(this).find(".checkbox-checked").css('display', 'block');
            checkbox_arr[field].push("'"+value+"'");
        }
        else
        {
            $(this).find(".checkbox-checked").css('display', 'none');
            checkbox_arr[field].splice(search_item(checkbox_arr[field], value), 1);
        }
        //console.log(checkbox_arr['width'].join(','));
        get_items(this);
    });

    //Показывает окно селекта
    $(".select").bind('click', function(){
        if($("#"+$(this).attr('data-window')+"-select").css('display') == 'none')
            $("#"+$(this).attr('data-window')+"-select").css('display', 'block');
        else
            $("#"+$(this).attr('data-window')+"-select").css('display', 'none');
    });

    //Выбираем знаечение селекта
    $(".select-window-value").bind('click', function() {
        change_ajax_select_value(this);
        return false;
    });

    //кнопка сброса параметра фильтра
    $(".reset").bind('click', function(){
        $(this).parent().find(".checkbox-checked").css('display', 'none');
        $(this).parent().find("input[type=text]").val('');
        $(this).parent().find(".select-value").empty().append('--------------');
        var type_filter = $(this).attr('data-reset');
        if(type_filter == 'width' || type_filter == 'collection' || type_filter== 'construction' || type_filter == 'mounting')
            checkbox_arr[type_filter] = [];
        if(type_filter == 'price')
        {
            query = query.replace(new RegExp(" AND price<=[0-9]*", "g"), '');
            query = query.replace(new RegExp(" AND price>=[0-9]*", "g"), '');
        }
        if(type_filter == 'color')
            query = query.replace(new RegExp(" AND color='[Рђ-РЇ Р°-СЏ]*'", "g"), '');
        get_items(this);
        return false;
    });

    $(".callback-btn").bind('click', function(){
        openform('mail-form', 285, 285);
        return false;
    });

    $(".close-form").bind('click', function() {
        $(this).parent().hide();
        $(".overlay").hide();
    });


    //paginator
    $(".pager li").bind('click', function(){
        pagerClick(this);
    });

    //Фильтр по цене
    $(".price").bind('input', function(){
        if($(this).attr('data-field') == 'price_from')
        {
            query = query.replace(new RegExp(" AND price>=[0-9]*", "g"), '');
            if($(this).val() != '') 
                query += " AND price>="+$(this).val();
        } else {
            query = query.replace(new RegExp(" AND price<=[0-9]*", "g"), '');
            if($(this).val() != '') 
                query += " AND price<="+$(this).val();
        }

        get_items(this);

    });
    //Сортировка каталога
    $(".sort-by img").bind('click', function(){
        $(this).attr('src', '/images/'+$(this).attr('data-sort')+'_active.png');
        if($(this).attr('data-sort') == 'sort_up')
        {
            order = 'price asc';
            var an_sort = 'sort_down';
        }
        else
        {
            var an_sort = 'sort_up';
            order = 'price desc';
        }
        $(".sort-by img[data-sort='"+an_sort+"']").attr('src', '/images/'+an_sort+'_non_active.png');
        get_items(null);
    });

    //Кнопка добавления в корзину
    $("#order-btn").bind('click', function(){
        $.ajax({
            url: '/ajax/add_to_cart',
            type: 'post',
            data: 'item_id='+$(this).attr('data-id'),
            success: function(data) {
                if(data == -1)
                   openform('success-form', 60, 285, 'ПОКУПКА ТОВАРА', 'В процессе произошла ошибка.'); 
                else
                {
                    var current_items = $(".cart-link span").text();
                    $(".cart-link span").empty().append(parseInt(current_items)+1); 
                    openform('success-form', 60, 285, 'ПОКУПКА ТОВАРА', 'Товар успешно добавлен в корзину.<div style="height:10px"></div><a href="/cart"><img src="/images/goto_cart.png"></a>&nbsp;&nbsp;&nbsp;<a href="#" onclick="close_form(this)"><img src="/images/goto_catalog.png"></a>');
                } 
            },
            error: function(data) {
                 openform('success-form', 60, 285, 'ПОКУПКА ТОВАРА', 'В процессе произошла ошибка.'); 
            }
        });
        return false;
    });

    //Удаление елемента из корзины
    $(".rem-item").bind('click', function(){
        $.ajax({
            url: '/ajax/del_from_cart',
            type: 'post',
            data: 'item_id='+$(this).attr('data-id'),
            success: function(data) {
                window.location.reload();
            }
        });
        return false;
    });
    //Адление всех элементов
    $("#del_all").bind('click', function(){
        $.ajax({
            url: '/ajax/del_from_cart',
            type: 'post',
            data: 'item_id=all',
            dataType: 'text',
            success: function() {
                window.location.reload();
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
        return false;
    });

    //Изменение количества товаров
    $(".cart-item-count").bind('input', function(){
        var val = $(this).val();
        var item_id = $(this).attr('item-id');
        $.ajax({
            url: '/ajax/ch_count',
            type: 'post',
            data: 'item_id='+item_id+'&count='+val,
            success: function(){
                window.location.reload();
            }
        });
    });

});

function  getPageSize(){
    var xScroll, yScroll;

    if (window.innerHeight && window.scrollMaxY) {
        xScroll = document.body.scrollWidth;
        yScroll = window.innerHeight + window.scrollMaxY;
    } else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
        xScroll = document.body.scrollWidth;
        yScroll = document.body.scrollHeight;
    } else if (document.documentElement && document.documentElement.scrollHeight > document.documentElement.offsetHeight){ // Explorer 6 strict mode
        xScroll = document.documentElement.scrollWidth;
        yScroll = document.documentElement.scrollHeight;
    } else { // Explorer Mac...would also work in Mozilla and Safari
        xScroll = document.body.offsetWidth;
        yScroll = document.body.offsetHeight;
    }

    var windowWidth, windowHeight;
    if (self.innerHeight) { // all except Explorer
        windowWidth = self.innerWidth;
        windowHeight = self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
        windowWidth = document.documentElement.clientWidth;
        windowHeight = document.documentElement.clientHeight;
    } else if (document.body) { // other Explorers
        windowWidth = document.body.clientWidth;
        windowHeight = document.body.clientHeight;
    }

    // for small pages with total height less then height of the viewport
    if(yScroll < windowHeight){
        pageHeight = windowHeight;
    } else {
        pageHeight = yScroll;
    }

    // for small pages with total width less then width of the viewport
    if(xScroll < windowWidth){
        pageWidth = windowWidth;
    } else {
        pageWidth = xScroll;
    }

    return [pageWidth,pageHeight,windowWidth,windowHeight];
}

function change_ajax_select_value(item) {
    $(".select[data-window="+$(item).attr('data-field')+"] .select-value").empty().append($(item).text());
    var El = $(".select[data-window="+$(item).attr('data-field')+"] .select-value");
    $("input[name='car["+$(item).attr('data-field')+"]']").attr('value', $(item).attr('data-id'));
    query = query.replace(new RegExp(" AND color='[Рђ-РЇ Р°-СЏ]*'", "g"), '');
    query += " AND color='"+$(item).attr('data-id')+"'";
    $(item).parent().css('display', 'none');
    get_items(El);
}

function show_pointer(El, count) {
    left_position = $(".catalog-inner").position().left
    top_position = $(El).position().top-10;
    $(".pointer").css({'top':top_position, 'left':left_position}).show(500);
    $(".pointer").empty().append("Найдено: "+count+" товаров.");
    setTimeout(function(){$(".pointer").hide(500)}, 1800);
}

function openform(form_name, form_top, form_left, title, content)
{
    jQuery("."+form_name).css('top', getPageSize()[3]/2-form_top);
    jQuery("."+form_name).css('left', getPageSize()[2]/2-form_left);
    jQuery("#popup-title").empty().append(title);
    jQuery("#popup-content").empty().append(content);
    jQuery("."+form_name).css('display', 'block');
    jQuery(".overlay").show();
}

function search_item(arr, value)
{
    for(i=0; i<arr.length; i++)
        if(arr[i] == value)
            return i
}

function get_items(El) {
    var width = '', collection = '', construction = '', mounting = '';
    if(checkbox_arr['width'].length != 0)
        width = " AND width IN ("+checkbox_arr['width'].join(',')+")";
    if(checkbox_arr['collection'].length != 0)
        collection = " AND complect_id IN ("+checkbox_arr['collection'].join(',')+")";
    if(checkbox_arr['construction'].length != 0)
        construction = " AND construction IN ("+checkbox_arr['construction'].join(',')+")";
    if(checkbox_arr['mounting'].length != 0)
        mounting = " AND mounting IN ("+checkbox_arr['mounting'].join(',')+")";
    var addition_param_name = '';
    var addition_param_value ='';
    if($("input[name=addition_param]"))
    {
        addition_param_name = $("input[name=addition_param]").attr('param-name');
        addition_param_value = $("input[name=addition_param]").attr('value');
    } else {
        addition_param_name = '';
        addition_param_value ='';
    }

    $.ajax({
        url: '/ajax/get_items',
        type: 'post',
        data: "&query_str="+query+width+collection+construction+mounting+"&order="+order+"&"+addition_param_name+"="+addition_param_value,
        success: function(data) {
            if(El != null)
                show_pointer(El, data.count);
            render_catalog(data);
        }
    });
}

function render_catalog(data) {
    var html = '';
    for(i=0; i<data.items.length; i++) {
            if(i%3==0)
            {
                if(i+1 > 12)
                    var hidden = 'hidden';
                else
                    var hidden = '';
                var page_id = parseInt((i+2)/12+1);
                html += '<ul class="catalog similar-items '+hidden+'" style="height: 335px;" page-id="'+page_id+'">';
            }
            html += '<li style="margin-left: 21px;">';
            html += '<a href="/catalog/'+data.items[i].id+'">';
            html += '<img src="/items/images/'+data.items[i].id+'/thumb_'+data.items[i].image_file_name+'">';
            html += '<div class="cart-item-header">'+data.items[i].name+'</div>';
            html += '<div class="item-info-text">'+data.items[i].cat_name+'</div>';
            html += '<div class="item-info-text">ширина '+data.items[i].width+' см</div>';
            html += '<div class="cart-item-price">'+data.items[i].price+' руб.</div>';
            html += '</a>';
            html += '</li>';
            if((i+1)%3 == 0 || i == data.items.length-1)
                html += '</ul>';
    }
    if(data.items.length > 12)
    {
        var pager = '';
        if(data.items.length%12 > 0)
            var end_of_list = parseInt(data.items.length/12)+1;
        else
            var end_of_list = parseInt(data.items.length/12);
        console.log(end_of_list);
        for(i=1; i<=end_of_list; i++)
        {
            if(i==1)
                var active = 'active';
            else
                var active ='';
            pager += '<li class="'+active+'" page-id="'+i+'" onclick="pagerClick(this)">'+i+'</li>';
            $(".pager").empty().html(pager).css('display', 'block');
        }
    }
    else
        $(".pager").css('display', 'none');
    $(".catalog-lists").empty().html(html);
}

function pagerClick(El) {
        $(".pager li").removeClass('active');
        $(El).addClass('active');
        var page = $(El).attr('page-id');
        $(".catalog").addClass('hidden');
        $(".catalog[page-id="+page+"]").removeClass('hidden');
}

function close_form(El) {
    $(El).parent().parent().hide();
    $(".overlay").hide();
    return false;
}