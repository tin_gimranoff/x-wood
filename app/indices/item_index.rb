ThinkingSphinx::Index.define :item, :with => :active_record do
  # fields
  indexes name
  indexes width
  indexes height 
  indexes depth
  indexes front_matherial
  indexes matherial
  indexes description
  indexes category.name, :as => :category, :sortable => true
  indexes complect.name, :as => :complect, :sortable => true

  # attributes
  has category_id, complect_id, created_at, updated_at
end