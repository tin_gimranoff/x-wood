class Getcall_footer
	include ActiveModel::Conversion
    include ActiveModel::AttributeMethods
    include ActiveModel::Validations
    extend  ActiveModel::Naming
    extend  ActiveModel::Translation

  attr_accessor :email, :message

  validates :email, :message, presence: true
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end
end