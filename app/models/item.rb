class Item < ActiveRecord::Base
	belongs_to :category
	belongs_to :complect

	has_attached_file :image, :styles => { :thumb => "239x241>", :inner => "470x470>" }, :path => ":rails_root/public/:class/:attachment/:id/:style_:basename.:extension", :url => "/:class/:attachment/:id/:style_:basename.:extension"
	validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

	validates :image, :name, :width, :height, :width, :depth, :front_matherial, :matherial, :garanty, :description, :price, :color, :mounting, :construction, presence: true
end
