#coding: utf-8
ActiveAdmin.register Order do

  menu label: "Заказы"
  permit_params :name, :surname, :fathername, :address, :phone, :comment, :content, :status
  index do
    selectable_column
    id_column
    column :name
    column :surname
    column :fathername
    column :comment
    column :phone
    column :status do |s|
      if s.status = 0
        "Новый"
      else
        "Просмотренный"
      end
    end
    actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  form(:html => {:multipart => true}) do |f|
      f.inputs "Новый заказ" do
        f.input :name
        f.input :surname
        f.input :fathername
        f.input :address
        f.input :phone
        f.input :comment
        f.input :status, as: :select, collection: [["Просмотренный", 1], ["Новый", 0]]
      end
      f.actions
  end
end
