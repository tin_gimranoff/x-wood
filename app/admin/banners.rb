#coding: utf-8
ActiveAdmin.register Banners do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end
  menu parent: "Структура"
  permit_params :label, :image, :url, :description

  index do
    selectable_column
    id_column
    column :description
    column :label
    column :url
    column :image do |banners|
      image_tag(banners.image.url)
    end
    column :created_at
    column :updated_at
    actions
  end

  form(:html => {:multipart => true}) do |f|
    f.inputs "Новый баннер" do
      f.input :description
      f.input :label
      f.input :image, :required => false, :as => :file
      f.input :url
    end
    f.actions
  end

end
