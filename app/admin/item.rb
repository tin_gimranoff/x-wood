#coding: utf-8
ActiveAdmin.register Item do

  menu parent: "Мебель"
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :image, :name, :width, :height, :width, :depth, :front_matherial, :matherial, :garanty, :description, :price, :old_price, :exist, :hit, :category_id, :complect_id, :color, :mounting, :construction

  index do
    selectable_column
    id_column
    column :name
    column :category
    column :complect
    column :price
    column :created_at
    column :updated_at
    actions
  end

  form(:html => {:multipart => true}) do |f|
      f.inputs "Новый товар" do
        f.input :image, :required => false, :as => :file
        f.input :name
        f.input :width
        f.input :height
        f.input :depth
        f.input :front_matherial
        f.input :matherial
        f.input :garanty
        f.input :description, :as => :ckeditor
        f.input :price
        f.input :old_price
        f.input :exist
        f.input :hit
        f.input :color
        f.input :mounting
        f.input :construction
        f.input :category_id, :as => :select, collection: Category.all
        f.input :complect_id, :as => :select, collection: Complect.all
      end
      f.actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
