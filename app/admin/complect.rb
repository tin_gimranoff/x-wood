#coding: utf-8
ActiveAdmin.register Complect do

  menu parent: "Мебель"

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :image

  index do
    selectable_column
    id_column
    column :name
    column :created_at
    column :updated_at
    actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  form(:html => {:multipart => true}) do |f|
      f.inputs "Новый комплект" do
        f.input :image, :required => false, :as => :file
        f.input :name
      end
      f.actions
  end
end
