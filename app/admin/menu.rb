ActiveAdmin.register Menu do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  menu parent: "Структура"
  permit_params :name, :url, :ordernum, :hide, :parent

  index do
    selectable_column
    id_column
    column :name
    column :url
    column :ordernum
    column :hide do |h|
      if h.hide == 0
        'Нет'
      else
        'Да'
      end
    end
    column :parent do |p|
      if !p.parent.blank?
        menu = Menu.find(p.parent)
        menu.name
      else
        '-'
      end
    end
    column :created_at
    column :updated_at
    actions
  end

  form(:html => {:multipart => true}) do |f|
    f.inputs "Новый элемент меню" do
      f.input :name
      f.input :url
      f.input :ordernum
      f.input :hide, as: :select, collection: [["Да", 1], ["Нет", 0]]
      f.input :parent, as: :select, collection: Menu.all
    end
    f.actions
  end
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
