#coding: utf-8
ActiveAdmin.register Vacancy do

  menu label: 'Вакансии'
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :name, :req, :cond, :price, :price_comment, :longtitude, :lattitude, :address
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
