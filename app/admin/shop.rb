ActiveAdmin.register Shop do

  menu label: "Магазины"

  index do
    selectable_column
    id_column
    column :name
    column :created_at
    column :updated_at
    actions
  end
  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :image, :name, :description, :longtitude, :lattitude
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

  form(:html => {:multipart => true}) do |f|
      f.inputs "Новый магазина" do
        f.input :image, :required => false, :as => :file
        f.input :name
        f.input :description, as: :ckeditor
        f.input :longtitude
        f.input :lattitude
      end
      f.actions
  end

end
