#coding: utf-8
class SiteMailer < ActionMailer::Base
  default from: "robot@x-wood.ru"

  def getcall_email(email, message)
  	@email = email
  	@message = message
  	mail(to: 'gimranov.valentin@yandex', subject: 'Новое сообщение с сайта') do |format|
      format.text { render "getcall_email" }
    end
  end

  def send_order(order)
    @order = order
  	mail(to: 'gimranov.valentin@yandex.ru', subject: 'Новый заказ') do |format|
      format.text { render "send_order" }
    end
  end

  def getcall_email_popup(form)
    @form = form
    mail(to: 'gimranov.valentin@yandex.ru', subject: 'Заявка на обратный звонок') do |format|
      format.text { render "getcall_popup" }
    end
  end 
end