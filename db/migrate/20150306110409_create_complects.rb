class CreateComplects < ActiveRecord::Migration
  def change
    create_table :complects do |t|
      t.string :name

      t.timestamps
    end
  end
end
