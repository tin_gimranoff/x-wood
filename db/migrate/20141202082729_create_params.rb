class CreateParams < ActiveRecord::Migration
  def change
    create_table :params do |t|
      t.string :param_name
      t.string :param_key
      t.string :param_value

      t.timestamps
    end
  end
end
