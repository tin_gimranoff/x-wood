class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :name
      t.string :surname
      t.string :fathername
      t.string :address
      t.string :phone
      t.text :comment
      t.text :content
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
