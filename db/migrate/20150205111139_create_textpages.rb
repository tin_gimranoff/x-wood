class CreateTextpages < ActiveRecord::Migration
  def change
    create_table :textpages do |t|
      t.string :name
      t.string :slug
      t.text :content

      t.timestamps
    end
  end
end
