class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :name
      t.has_attached_file :image
      t.text :description
      t.string :longtitude
      t.string :lattitude

      t.timestamps
    end
  end
end
