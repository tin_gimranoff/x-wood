class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.has_attached_file :image
      t.string :label
      t.string :url

      t.timestamps
    end
  end
end
