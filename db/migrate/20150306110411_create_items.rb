class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.has_attached_file :image
      t.string :name
      t.integer :width
      t.integer :height
      t.integer :depth
      t.string :front_matherial
      t.string :matherial
      t.integer :garanty
      t.text :description
      t.integer :price
      t.integer :old_price
      t.boolean :exist, :default => true
      t.boolean :hit
      t.references :category
      t.references :complect

      t.timestamps
    end
  end
end
