class AddImageToComplect < ActiveRecord::Migration
  def change
    add_attachment :complects, :image
  end
end
