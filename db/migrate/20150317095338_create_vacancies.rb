class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.string :req
      t.string :cond
      t.string :price
      t.string :price_comment
      t.string :address
      t.string :longtitude
      t.string :lattitude

      t.timestamps
    end
  end
end
