class AddColumnsToItem < ActiveRecord::Migration
  def change
    add_column :items, :color, :string
    add_column :items, :construction, :string
    add_column :items, :mounting, :string
  end
end
